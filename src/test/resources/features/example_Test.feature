Feature: Test the login

  Scenario: Test if the login gives an error msg when the wrong credentials are entered now
    Given user is on the login page
    When user tries to login with a valid username and a wrong pw
    Then error msg is shown on one line