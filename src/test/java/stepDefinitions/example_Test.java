package stepDefinitions;

import static org.junit.Assert.assertEquals;

import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.JsonNode;
import com.mashape.unirest.http.exceptions.UnirestException;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class example_Test {
    String apiUrl = "https://jsonplaceholder.typicode.com/todos/";
    HttpResponse<JsonNode> firstTodoResponse;

    @Given("^user is on the login page$")
    public void the_todo_api_is_up() throws UnirestException {
        
    }

    @When("^user tries to login with a valid username and a wrong pw$")
    public void i_request_the_first_todo() throws  UnirestException {
        
    }

    //@Then("^error msg is shown on one line$")
    //public void the_first_todo_is_returned() {
        
    //}
}